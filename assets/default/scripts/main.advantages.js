;(function($, window, document, undefined) {
	'use strict';

	main.advantages = {
		wrapper: '.advantages',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.find('.container').carouFredSel({
				items: {
					visible: 1,
					minimum: 2
				},
				scroll: {
					fx: 'crossfade'
				},
				auto: {
					timeoutDuration: 3000
				},
				prev: $wrapper.find('.prev'),
				next: $wrapper.find('.next'),
				responsive: true
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
			}
		}
	};
}(jQuery, this, this.document));
