;(function($, window, document, undefined) {
	'use strict';

	main.collapse = {
		wrapper: '.collapse-main',

		bindings: function() {
			var $wrapper = $(main.collapse.wrapper);

			$wrapper.find('.collapse-main-heading').on({
				click: function(e) {
					var	$parent = $(this).closest(main.collapse.wrapper),
						$icon = $(this).find('span'),
						open_class = 'collapse-main-open';

					if ($parent.hasClass(open_class)) {
						$parent.removeClass(open_class)
						$icon.text('+');
					} else {
						$parent.addClass(open_class)
						$icon.text('-');
					}

					e.preventDefault();
				}
			});
		},

		init: function() {
			var $wrapper = $(main.collapse.wrapper);

			if ($wrapper.length) {
				$('.collapse-main-open .collapse-main-heading span').text('-');

				main.collapse.bindings();
			}
		}
	};
}(jQuery, this, this.document));
