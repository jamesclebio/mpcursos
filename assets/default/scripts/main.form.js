;(function($, window, document, undefined) {
	'use strict';

	main.form = {
		masks: function() {
			$('.mask-date').mask('99/99/9999');
			$('.mask-phone').mask('(99) 9999-9999');
			$('.mask-postal').mask('99999-999');
			$('.mask-cpf').mask('999.999.999-99');
		},

		cep: function() {
			var $input = $('[name="cep"]');

			function getAddress(cep) {
				cep = cep.replace(/\D/g,'');

				$.ajax({
					url: 'http://cep.correiocontrol.com.br/' + cep + '.json',

					beforeSend: function() {
						var $alert = $input.next('.errorList');

						$input.addClass('input-loading');

						if ($alert.length)
							$alert.remove();
					},

					error: function() {
						// $input.after('<ul class="errorList"><li>CEP Inválido! Tente novamente.</li></ul>');
						// $('[name="endereco"], [name="bairro"], [name="cidade"], [name="estado"]').val('');
					},

					success: function(data) {
						$('[name="endereco"]').val(data.logradouro);
						$('[name="bairro"]').val(data.bairro);
						$('[name="cidade"]').val(data.localidade);
						$('[name="estado"]').val(data.uf);
						$('[name="numero"]').focus();
					},

					complete: function() {
						$input.removeClass('input-loading');
					}
				});
			}

			$input.on({
				blur: function() {
					getAddress($(this).val());
				}
			});
		},

		chosen: function() {
			var $wrapper = $('select.chosen');

			if ($wrapper.length)
				$wrapper.chosen();
		},

		bindings: function() {
			var $videotest = $('[name="videotest"]');

			$videotest.on({
				change: function() {
					if ($(this).is(':checked')) {
						$videotest.prop('checked', true);
					} else {
						$videotest.prop('checked', false);
					}
				}
			});
		},

		init: function() {
			main.form.masks();
			main.form.cep();
			main.form.chosen();
			main.form.bindings();
		}
	};
}(jQuery, this, this.document));
