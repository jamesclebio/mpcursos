;(function($, window, document, undefined) {
	'use strict';

	main.gallery = {
		wrapper: '.gallery',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper),
				$links = $wrapper.find('a');

			$links.attr('rel', 'gallery').fancybox();
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
			}
		}
	};
}(jQuery, this, this.document));
