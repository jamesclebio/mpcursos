;(function($, window, document, undefined) {
	'use strict';

	main.giftcard = {
		wrapper: '#form-giftcard',

		validate: function() {
			var	that = this,
				$form = $(that.wrapper),
				$alert = $form.find('.alert'),
				url = $form.attr('action'),
				data = $form.serialize();

			function resultError(message) {
				$alert
					.removeClass('alert-loading')
					.html(message)
					.show();
			}

			function resultSuccess(url) {
				var	$parent = $form.parent();

				$.ajax({
					type: 'GET',
					url: url,

					error: function() {
						alert('Ops! Algo deu errado... :(');
					},

					success: function(data) {
						$parent.empty().append(data);
					}
				});
			}

			$.ajax({
				type: 'POST',
				url: url,
				data: data,
				dataType: 'json',

				beforeSend: function() {
					$alert
						.addClass('alert-loading')
						.html('Validando cartão...')
						.show();
				},

				error: function() {
					resultError();
				},

				success: function(data) {
					(data.error) ? resultError(data.error) : resultSuccess(data.url);
				}
			});
		},

		bindings: function() {
			var	that = this;

			$(document).on({
				submit: function(e) {
					that.validate();
					e.preventDefault();
				}
			}, that.wrapper);
		},

		init: function() {
			var	that = this;

			that.bindings();
		}
	};
}(jQuery, this, this.document));
