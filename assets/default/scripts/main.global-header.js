;(function($, window, document, undefined) {
	'use strict';

	main.globalHeader = {
		login: function() {
			var	$form = $('#form-signin'),
				$alert = $form.find('.alert'),
				$links = $form.find('.links'),
				url = $form.attr('action'),
				data = $form.serialize();

			function alertError() {
				$alert
					.removeClass('alert-loading')
					.html('<strong>Dados inválidos!</strong> Tente novamente.')
					.delay(3000).fadeOut(100, function() {
						$links.show();
					});
			}

			$.ajax({
				type: 'POST',
				url: url,
				data: data,

				beforeSend: function() {
					$links.hide();
					$alert
						.addClass('alert-loading')
						.html('<strong>Acessando sua conta...</strong>')
						.show();
				},

				error: function() {
					alertError();
				},

				success: function(data) {
					(data == '') ? alertError() : document.location = data;
				}
			});
		},

		bindings: function() {
			var	$wrapper = $('.global-header');

			// Sign in
			$wrapper.find('#form-signin').on({
				submit: function(e) {
					main.globalHeader.login();
					e.preventDefault();
				}
			});

			// Logged nav
			$wrapper.find('.logged > ul > li > a').on({
				mouseenter: function() {
					var sub = $(this).next('ul');

					if (sub.length && !sub.is(':visible')) {
						$wrapper.find('ul ul').hide();
						$(this).addClass('active');
						sub.clearQueue().fadeIn(0);
					}
				},

				mouseleave: function() {
					var sub = $(this).next('ul');

					if (sub.is(':visible')) {
						sub.delay(10).fadeOut(0, function() {
							sub.prev('a').removeClass('active');
						});
					}
				}
			});

			$wrapper.find('.logged ul ul').on({
				mouseenter: function() {
					$(this).clearQueue();
				},

				mouseleave: function() {
					$(this).prev('a').removeClass('active');
					$(this).fadeOut(0);
				}
			});
		},

		init: function() {
			main.globalHeader.bindings();
		}
	};
}(jQuery, this, this.document));
