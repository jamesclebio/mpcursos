;(function($, window, document, undefined) {
	'use strict';

	main.globalNav = {
		columns: function() {
			var	$wrapper = $('.global-nav'),
				$subs = $wrapper.find('ul ul'),
				column_items = 7, // 1 for inline order
				column_counter = 1;

			$subs.each(function() {
				var	$items = $(this).find('li');

				if ($items.length > column_items && $items.length <= (column_items * 2))
					$(this).width(440);
				else if ($items.length > (column_items * 2))
					$(this).width(650);

				if ($(this).closest('li').offset().left <= (window.innerWidth / 2))
					$(this).css('left', '-25px');
				else
					$(this).css('right', '-25px');

				$items.each(function() {
					$(this).addClass('column-' + column_counter + '');

					if (($(this).index() + 1) % column_items == 0 || $(this).is(':last-child'))
						column_counter++;
				});
			});

			for (var i = 1; i <= column_counter; i++) {
				$subs.find('.column-' + i + '').wrapAll('<span class="column" style="width: 210px; float: left;" />');
			}
		},

		bindings: function() {
			var	$wrapper = $('.global-nav');

			$wrapper.find('> ul > li > a').on({
				mouseenter: function() {
					var sub = $(this).next('ul');

					if (sub.length && !sub.is(':visible')) {
						$wrapper.find('ul ul').hide();
						$(this).addClass('active');
						sub.clearQueue().fadeIn(0);
					}
				},

				mouseleave: function() {
					var sub = $(this).next('ul');

					if (sub.is(':visible')) {
						sub.delay(10).fadeOut(0, function() {
							sub.prev('a').removeClass('active');
						});
					}
				}
			});

			$wrapper.find('ul ul').on({
				mouseenter: function() {
					$(this).clearQueue();
				},

				mouseleave: function() {
					$(this).prev('a').removeClass('active');
					$(this).fadeOut(0);
				}
			});
		},

		init: function() {
			var	$wrapper = $('.global-nav');

			main.globalNav.columns();
			main.globalNav.bindings();
		}
	};
}(jQuery, this, this.document));
