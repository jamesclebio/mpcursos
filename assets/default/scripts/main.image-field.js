;(function($, window, document, undefined) {
	'use strict';

	main.imageField = {
		wrapper: '[data-image-field]',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.each(function() {
				var	$field = $(this).clone(true),
					$label = $(this).closest('label'),
					$error_list = $label.find('.errorList'),
					image = ($label.find('a').length) ? $label.find('a').attr('href') : $(this).data('imageField'),
					template_error_list = ($error_list.length) ? '<ul class="errorList">' + $error_list.html() + '</ul>' : '',
					template_image_field = '
						Imagem *
						<div class="image-field">
							<div class="preview">
								<img src="' + image + '" alt="Preview">
							</div>
							' + $field.context.outerHTML + '
							<div class="note">Selecione uma imagem JPG ou PNG, com 174 x 215, de até 2MB.</div>
							' + template_error_list + '
						</div>
					';

				$label.empty().append(template_image_field);
			});
		},

		loadFile: function(field) {
			var	that = this,
				$wrapper = $(field).closest('.image-field'),
				$preview = $wrapper.find('.preview img'),
				template_error = '<ul class="errorList"><li>O arquivo selecionado é inválido.</li></ul>',
				reader;

			if (field.files && field.files[0]) {
				reader = new FileReader();

				function resultError() {
					var	$error_list = $wrapper.find('.errorList');

					if ($error_list.length) {
						$error_list.remove()
					}

					$(field).val('');
					$wrapper.append(template_error);
				}

				reader.onload = function(e) {
					if (/^data:image\/(jpeg|png)/i.test(e.target.result)) {
						$preview.attr('src', e.target.result);
					} else {
						resultError();
					}
				}

				reader.readAsDataURL(field.files[0]);
			}
		},

		bindings: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			$wrapper.on({
				change: function() {
					that.loadFile(this);
				}
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
				that.bindings();
			}
		}
	};
}(jQuery, this, this.document));
