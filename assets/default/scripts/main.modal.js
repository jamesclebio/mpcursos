;(function($, window, document, undefined) {
	'use strict';

	main.modal = {
		wrapper: '.modal-wrapper',
		template: '<div class="modal-wrapper" style="display: none;"><div class="modal-container"><div class="modal-box"><a href="#" class="modal-close" title="Fechar">x</a></div></div></div>',

		open: function(url) {
			$('body').append(main.modal.template);

			$(main.modal.wrapper).fadeIn(100, function() {
				var	$container = $(main.modal.wrapper).find('.modal-box');

				$.ajax({
					url: url,

					beforeSend: function() {
						$container.addClass('modal-loading');
					},

					error: function() {
						alert('Ops! Algo deu errado. Tente novamente.');

						main.modal.close();
					},

					success: function(data) {
						$container
							.removeClass('modal-loading')
							.prepend(data);
					}
				});
			});
		},

		close: function() {
			$(main.modal.wrapper).fadeOut(100, function() {
				$(this).remove();
			});
		},

		bindings: function() {
			$(document).on({
				click: function() {
					main.modal.close();
				}
			}, main.modal.wrapper);

			$(document).on({
				click: function(e) {
					e.stopPropagation();
				}
			}, main.modal.wrapper + ' .modal-box');

			$(document).on({
				click: function(e) {
					main.modal.close();
					e.preventDefault();
				}
			}, main.modal.wrapper + ' .modal-close');

			$(document).on({
				click: function(e) {
					var url = $(this).data('url');

					main.modal.open(url);
					e.preventDefault();
				}
			}, '.modal-open');
		},

		init: function() {
			main.modal.bindings();
		}
	};
}(jQuery, this, this.document));
