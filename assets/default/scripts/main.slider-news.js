;(function($, window, document, undefined) {
	'use strict';

	main.sliderNews = {
		wrapper: '.slider-news',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper),
				template_index = '<div class="index" />';

			$wrapper.append(template_index);

			$wrapper.find('.items').carouFredSel({
				items: {
					visible: 1,
					minimum: 2
				},
				scroll: {
					fx: 'crossfade'
				},
				auto: {
					timeoutDuration: 4000
				},
				pagination: $wrapper.find('.index'),
				responsive: true
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
			}
		}
	};
}(jQuery, this, this.document));
