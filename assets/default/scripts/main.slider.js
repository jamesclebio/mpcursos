;(function($, window, document, undefined) {
	'use strict';

	main.slider = {
		wrapper: '.global-slider',

		build: function() {
			var	that = this,
				$wrapper = $(that.wrapper),
				template_prevnext = '<div class="nav"><a href="#" class="prev">prev</a><a href="#" class="next">next</a></div>';

			$wrapper.find('.global-slider-container').append(template_prevnext);

			$wrapper.find('.items').carouFredSel({
				items: {
					visible: 1,
					minimum: 2
				},
				scroll: {
					fx: 'crossfade'
				},
				auto: {
					timeoutDuration: 4000
				},
				prev: $wrapper.find('.nav .prev'),
				next: $wrapper.find('.nav .next'),
				responsive: true
			});
		},

		init: function() {
			var	that = this,
				$wrapper = $(that.wrapper);

			if ($wrapper.length) {
				that.build();
			}
		}
	};
}(jQuery, this, this.document));
