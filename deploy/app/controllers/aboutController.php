<?php
class About extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('about');
	}
}
