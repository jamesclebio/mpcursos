<?php
class Checkout extends Page
{
	public function __construct() {
		$this->setLayout('checkout');
		$this->setView(false);
	}

	public function purchase() {
		switch ($this->_get('step')) {
			case 1:
				$this->setView('checkout-login');
				break;

			case 2:
				$this->setView('checkout-pay');
				break;

			case 3:
				$this->setView('checkout-confirm');
				break;
		}
	}

	public function pay() {
		$this->setLayout(false);
		$this->setView('includes/pay');
	}

	public function giftcard() {
		$this->setLayout(false);

		switch ($this->_get('check')) {
			case 1:
				$this->setView('checkout-giftcard');
				break;

			case 2:
				$this->setView('includes/giftcard');
				break;

			case 3:
				$this->setView('checkout-giftcard');
				break;
		}
	}
}
