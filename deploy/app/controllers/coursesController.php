<?php
class Courses extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('courses');
	}

	public function test() {
		$this->setView('courses-test');
	}
}
