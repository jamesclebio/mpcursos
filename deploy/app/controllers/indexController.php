<?php
class Index extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('index');
		$this->setHtmlClass('layout-home');
		$this->slider = true;
		$this->splash = false;
	}
}
