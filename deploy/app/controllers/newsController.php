<?php
class News extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('news');
	}

	public function view() {
		$this->news_view = true;
	}
}
