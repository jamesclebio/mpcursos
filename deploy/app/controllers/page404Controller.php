<?php
class Page404 extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('404');
	}
}
