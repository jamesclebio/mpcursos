<?php
class Page500 extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('500');
	}
}
