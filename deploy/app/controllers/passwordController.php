<?php
class Password extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView(false);
	}

	public function reset() {
		$this->setView('password-reset');
	}

	public function remember() {
		$this->setLayout(false);
		$this->setView('password-remember');
	}
}
