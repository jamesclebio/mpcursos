<?php
class Product extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('product');
	}

	public function courses() {
		$this->setView('category');
		$this->product_category = 'Cursos';
	}

	public function trainings() {
		$this->setView('category');
		$this->product_category = 'Treinamentos';
	}
}
