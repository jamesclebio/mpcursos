<?php
class Sign extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView(false);
	}

	public function in() {
		$this->setLayout(false);
		$this->setView('includes/sign-in');
	}

	public function up() {
		$this->setView('sign-up');
	}
}
