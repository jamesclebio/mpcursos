<?php
class Support extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('support');
	}

	public function edit() {
		$this->setView('support-edit');
	}
}
