<?php
class Teacher extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView(false);
	}

	public function all() {
		$this->setView('teacher-all');
	}

	public function profile() {
		$this->setView('teacher-profile');
	}
}
