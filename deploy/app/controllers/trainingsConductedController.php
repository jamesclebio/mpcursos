<?php
class TrainingsConducted extends Page
{
	public function __construct() {
		$this->setLayout('default');
		$this->setView('trainings-conducted');
	}

	public function view() {
		$this->trainings_view = true;
	}
}
