<!doctype html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?> no-js layout-buy" lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>MP Cursos</title>
	<link rel="stylesheet" href="<?php echo $this->_asset('default/styles/main.css'); ?>">
	<script src="<?php echo $this->_asset('default/scripts/init.js'); ?>"></script>
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
	<?php $this->getAnalytics(); ?>
	<?php $this->getBodyPrepend(); ?>

	<header class="global-header">
		<div class="global-header-container">
			<h1 class="logo"><a href="<?php echo $this->_url('root'); ?>">MP Cursos</a></h1>

			<a href="<?php echo $this->_url('cart'); ?>" class="cart">
				<h5>Meu Carrinho</h5>
				<span>299 cursos</span>
			</a>
		</div>
	</header>

	<div class="buy-steps">
		<div class="step step-highlight">Identificação <span>1</span></div>
		<div class="step <?php echo ($this->_get('step') >= 2 ? 'step-highlight' : '') ?>">Pagamento <span>2</span></div>
		<div class="step <?php echo ($this->_get('step') >= 3 ? 'step-highlight' : '') ?>">Confirmação <span>3</span></div>
	</div>

	<div class="global-content">
		<?php $this->getView(); ?>
	</div>

	<script src="<?php echo $this->_asset('default/scripts/main.js'); ?>"></script>
	<?php $this->getBodyAppend(); ?>
</body>
</html>
