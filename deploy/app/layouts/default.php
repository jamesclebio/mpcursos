<!doctype html>
<html<?php $this->getHtmlAttribute(); ?> class="<?php $this->getHtmlClass(); ?> no-js" lang="pt-br">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>MP Cursos</title>
	<link rel="stylesheet" href="<?php echo $this->_asset('default/styles/main.css'); ?>">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<script src="<?php echo $this->_asset('default/scripts/init.js'); ?>"></script>
</head>
<body<?php $this->getBodyAttribute(); ?> class="<?php $this->getBodyClass(); ?>">
	<?php $this->getAnalytics(); ?>
	<?php $this->getBodyPrepend(); ?>

	<header class="global-header">
		<div class="global-header-container">
			<h1 class="logo"><a href="<?php echo $this->_url('root'); ?>">MP Cursos</a></h1>

			<h3 class="welcome">Olá, seja bem-vindo!</h3>

			<form id="form-search" method="get" action="" class="search">
				<fieldset>
					<legend>Pesquisa</legend>
					<input name="q" type="text" placeholder="O que você procura?">
					<button type="submit">Pesquisar</button>
				</fieldset>
			</form>

			<a href="<?php echo $this->_url('certified'); ?>" class="certified">
				<h5>Autenticidade de diploma</h5>
				<span>Clique para verificar</span>
			</a>

			<a href="#" data-url="<?php echo $this->_url('checkout/giftcard/check/1'); ?>" class="giftcard modal-open">
				<h5>Cartão presente</h5>
				<span>Clique para usar</span>
			</a>

			<a href="<?php echo $this->_url('cart'); ?>" class="cart">
				<h5>Meu Carrinho</h5>
				<span>299 cursos</span>
			</a>

			<div class="session">
				<!-- <h4>James Clébio</h4>
				<div class="logged">
					<div class="lastaccess">Último acesso em<br> 99/99/9999 99:99:99</div>
					<ul>
						<li class="account"><a href="#">Minha Conta</a>
							<ul>
								<li><a href="<?php echo $this->_url('courses'); ?>">Meus Cursos</a></li>
								<li><a href="<?php echo $this->_url('support'); ?>">Material de Suporte</a></li>
								<li><a href="<?php echo $this->_url('purchases'); ?>">Minhas Compras</a></li>
								<li><a href="<?php echo $this->_url('sales'); ?>">Minhas Vendas</a></li>
								<li><a href="<?php echo $this->_url('billing'); ?>">Meu Extrato</a></li>
								<li><a href="<?php echo $this->_url('account'); ?>">Meus Dados</a></li>
							</ul>
						</li>
						<li class="logout"><a href="#">Sair</a></li>
					</ul>
				</div> -->

				<h4>Área do Aluno</h4>
				<form id="form-signin" method="post" action="<?php echo $this->_url('sign/in'); ?>">
					<fieldset>
						<legend>Login</legend>
						<input name="user" type="text" placeholder="Usuário" required>
						<input name="password" type="password" placeholder="Senha" required>
						<button type="submit">Entrar</button>
						<div class="util">
							<div class="alert"></div>
							<ul class="links">
								<li><a href="#" data-url="<?php echo $this->_url('password/remember'); ?>" class="modal-open">Esqueceu a senha?</a></li>
								<li><a href="<?php echo $this->_url('sign/up'); ?>"><strong>Cadastre-se!</strong></a></li>
							</ul>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</header>

	<nav class="global-nav">
		<ul>
			<li><a href="<?php echo $this->_url('root'); ?>">Home</a></li>
			<li><a href="<?php echo $this->_url('about'); ?>">Institucional</a></li>
			<li><a href="<?php echo $this->_url('product/courses'); ?>">Cursos</a>
				<ul>
					<li><a href="#">Categoria A (1)</a></li>
					<li><a href="#">Categoria B (2)</a></li>
					<li><a href="#">Categoria C (3)</a></li>
					<li><a href="#">Categoria D (4)</a></li>
					<li><a href="#">Categoria E (5)</a></li>
					<li><a href="#">Categoria F (6)</a></li>
					<li><a href="#">Categoria G (7)</a></li>
					<li><a href="#">Categoria H (8)</a></li>
					<li><a href="#">Categoria I (9)</a></li>
					<li><a href="#">Categoria J (10)</a></li>
					<li><a href="#">Categoria K (11)</a></li>
					<li><a href="#">Categoria L (12)</a></li>
				</ul>
			</li>
			<li><a href="<?php echo $this->_url('product/trainings'); ?>">Treinamentos</a>
				<ul>
					<li><a href="#"><strong>Treinamentos realizados</strong></a></li>
					<li><a href="#">Categoria A (1)</a></li>
					<li><a href="#">Categoria B (2)</a></li>
					<li><a href="#">Categoria C (3)</a></li>
					<li><a href="#">Categoria D (4)</a></li>
					<li><a href="#">Categoria E (5)</a></li>
					<li><a href="#">Categoria F (6)</a></li>
					<li><a href="#">Categoria G (7)</a></li>
					<li><a href="#">Categoria H (8)</a></li>
					<li><a href="#">Categoria I (9)</a></li>
					<li><a href="#">Categoria J (10)</a></li>
					<li><a href="#">Categoria K (11)</a></li>
					<li><a href="#">Categoria L (12)</a></li>
				</ul>
			</li>
			<li><a href="<?php echo $this->_url('news'); ?>">Notícias</a></li>
			<li><a href="<?php echo $this->_url('contact'); ?>">Contato</a></li>
			<li class="signup"><a href="<?php echo $this->_url('sign/up'); ?>">Inscreva-se já!</a></li>
		</ul>
	</nav>

	<?php if ($this->slider): ?>
		<div class="global-slider">
			<div class="global-slider-container">
				<ul class="items">
					<li><a href="#1"><img src="<?php echo $this->_asset('default/images/slider/projeto-saber.jpg'); ?>" alt=""></a></li>
					<li><a href="#2"><img src="<?php echo $this->_asset('default/images/slider/projeto-saber.jpg'); ?>" alt=""></a></li>
				</ul>
			</div>
		</div>
	<?php endif; ?>

	<div class="global-content">
		<?php $this->getView(); ?>
	</div>

	<footer class="global-footer">
		<div class="shorts">
			<div class="grid grid-items-2">
				<div class="grid-item">
					<h3>Nossos Cursos</h3>
					<div class="grid grid-items-2">
						<div class="grid-item">
							<ul>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
							</ul>
						</div>
						<div class="grid-item">
							<ul>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
							</ul>
						</div>
					</div>
				</div>
				<div class="grid-item">
					<h3>Nossos Treinamentos</h3>
					<div class="grid grid-items-2">
						<div class="grid-item">
							<ul>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
							</ul>
						</div>
						<div class="grid-item">
							<ul>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
								<li><a href="#">Lorem ipsum dolor sit</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="payment">
			<a href="https://pagseguro.uol.com.br/para_voce/meios_de_pagamento_e_parcelamento.jhtml#rmcl" target="_blank" class="methods"><img src="<?php echo $this->_asset('default/images/payments.png'); ?>" alt="Formas de pagamento"></a>
			<a href="https://pagseguro.uol.com.br/para_voce/protecao_contra_fraudes.jhtml#rmcl" target="_blank" class="pagseguro"><img src="<?php echo $this->_asset('default/images/pagseguro.png'); ?>" alt="PagSeguro"></a>
		</div>

		<div class="copyright">
			<a href="<?php $this->_url('root'); ?>" class="mpcursos">MP Cursos</a>
			<address>
				Avenida Otaviano Leandro de Morais, 499 - 1º Andar<br>
				CEP: 48600-000 - Paulo Afonso/BA<br>
				<a href="tel:07532811180">(75) 3281-1180</a> / <a href="tel:07591209311">(75) 9120-9311</a>
			</address>
			<a href="http://www.agw.com.br" target="_blank" class="agw">AGW Internet</a>
		</div>
	</footer>

	<?php if ($this->splash): ?>
	<div class="splash-main splash-main-type-1" data-blank="true" data-timeout="7000" data-close="true">
		<div class="content">
			<a href="#"><img src="<?php echo $this->_asset('default/images/splashes/promocao-compartilhe-e-ganhe.jpg'); ?>" alt="Promoção Compartilhe e Ganhe"></a>
		</div>
	</div>
	<?php endif; ?>

	<script src="<?php echo $this->_asset('default/scripts/main.js'); ?>"></script>
	<?php $this->getBodyAppend(); ?>
</body>
</html>
