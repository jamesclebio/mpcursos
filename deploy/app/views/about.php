<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Institucional</h3>

<div class="grid grid-items-2">
	<div class="grid-item">
		<div class="text-body">
			<h4>Nossa empresa</h4>
			<p>Estamos localizado no eixo das Construções do Nordeste, na cidade de Paulo Afonso/BA, ministrando cursos em todo o Brasil.</p>
			<p>Pensando nisso, MP CURSOS, vem se dedicando cada vez mais em Cursos Livres (curtos, rápidos, necessários e que atendem aos anseios pessoas e empresariais). Com isso, criamos os CURSOS DE MÁQUINAS PESADAS, uma vez que possuimos uma equipe com vasta experiência sendo representado por um Operador de Máquinas Pesadas, Encarregado de Terraplanagem e Engenheiro de Segurança com anos de experiência e sendo o pioneiro a oferecer o curso intensivo de Operador de Máquinas Pesadas no Brasil, onde, constamos a grande carência de profissionais dessa área a nível nacional.</p>
			<p>Uma semana de acompanhamento avaliativo da equipe MP CURSO, atestando fidelidade e compromisso dos treinamentos prestados junto ao alunado, corrigindo-os se necessário em caso de qualquer deficiência técnica, garantindo a eficácia junto aos serviços executados.</p>
		</div>
	</div>

	<div class="grid-item">
		<div class="text-body">
			<h4>Aulas práticas</h4>
			<p>Nossas aulas práticas são realizadas em simuladores, evitando prejuízos à prefeitura e danos aos equipamentos em caso de alguma operação indevida realizada pelo aluno. </p>

			<h4>Inovação</h4>
			<p>O curso conta também com o conteúdo Manutenção Preventiva, quem vem prolongando a vida útil dos componentes e proporcionando um menor tempo de parada dos equipamentos.</p>
			<p>Portanto é com plena satisfação quem multiplicamos a nossa filosofia: </p>
			<div class="box-well a-center">
				<p><strong>"Só o trabalho e o conhecimento produz e faz história."</strong></p>
			</div>
		</div>
	</div>
</div>

<div class="m-top-15">
	<h4 class="heading-box heading-box-blue">Galeria de imagens</h4>
	<ul class="gallery list-thumb">
		<li><a href="<?php echo $this->_asset('default/images/about/01.jpg'); ?>" title=""><img src="<?php echo $this->_asset('default/images/about/thumb/01.jpg'); ?>" alt=""></a></li>
		<li><a href="<?php echo $this->_asset('default/images/about/02.jpg'); ?>" title=""><img src="<?php echo $this->_asset('default/images/about/thumb/02.jpg'); ?>" alt=""></a></li>
		<li><a href="<?php echo $this->_asset('default/images/about/03.jpg'); ?>" title=""><img src="<?php echo $this->_asset('default/images/about/thumb/03.jpg'); ?>" alt=""></a></li>
		<li><a href="<?php echo $this->_asset('default/images/about/04.jpg'); ?>" title=""><img src="<?php echo $this->_asset('default/images/about/thumb/04.jpg'); ?>" alt=""></a></li>
		<li><a href="<?php echo $this->_asset('default/images/about/05.jpg'); ?>" title=""><img src="<?php echo $this->_asset('default/images/about/thumb/05.jpg'); ?>" alt=""></a></li>
		<li><a href="<?php echo $this->_asset('default/images/about/06.jpg'); ?>" title=""><img src="<?php echo $this->_asset('default/images/about/thumb/06.jpg'); ?>" alt=""></a></li>
		<li><a href="<?php echo $this->_asset('default/images/about/07.jpg'); ?>" title=""><img src="<?php echo $this->_asset('default/images/about/thumb/07.jpg'); ?>" alt=""></a></li>
		<li><a href="<?php echo $this->_asset('default/images/about/08.jpg'); ?>" title=""><img src="<?php echo $this->_asset('default/images/about/thumb/08.jpg'); ?>" alt=""></a></li>
		<li><a href="<?php echo $this->_asset('default/images/about/09.jpg'); ?>" title=""><img src="<?php echo $this->_asset('default/images/about/thumb/09.jpg'); ?>" alt=""></a></li>
		<li><a href="<?php echo $this->_asset('default/images/about/10.jpg'); ?>" title=""><img src="<?php echo $this->_asset('default/images/about/thumb/10.jpg'); ?>" alt=""></a></li>
		<li><a href="<?php echo $this->_asset('default/images/about/11.jpg'); ?>" title=""><img src="<?php echo $this->_asset('default/images/about/thumb/11.jpg'); ?>" alt=""></a></li>
		<li><a href="<?php echo $this->_asset('default/images/about/12.jpg'); ?>" title=""><img src="<?php echo $this->_asset('default/images/about/thumb/12.jpg'); ?>" alt=""></a></li>
	</ul>
</div>
