<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Meus Dados</h3>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<!-- Form -->
<form id="form-signup" method="post" action="" class="form-main m-top-30">
	<fieldset>
		<legend>Cadastro</legend>
		<div class="wrapper">
			<div class="grid-content-half f-left">
				<h4 class="heading-group">Dados pessoais</h4>
				<label>Nome completo *<input name="nome" type="text" required readonly></label>
				<label>CPF *<input name="cpf" type="text" class="mask-cpf" required readonly></label>
				<label>RG *<input name="cpf" type="text" class="mask-cpf" required readonly></label>
				<label>E-mail *<input name="email" type="email" required readonly></label>
				<label>Telefone *<input name="email" type="text" class="mask-phone" required></label>
				<div class="wrapper">
					<div class="w-210 f-left">
						<label>Data de Nascimento *<input name="nascimento" type="text" class="mask-date" required></label>
					</div>
					<div class="w-210 f-right">
						<label>Sexo *
							<select name="sexo" required>
								<option value="m">Masculino</option>
								<option value="f">Feminino</option>
							</select>
						</label>
					</div>
				</div>
				<label>Nova Senha *<input name="senha" type="password" required></label>
				<label>Confirme a Nova Senha *<input name="senha_conf" type="password" required></label>

				<h4 class="heading-group">Perfil</h4>
				<label>Imagem *
					<input id="id_foto" name="foto" type="file" data-image-field="<?php echo $this->_asset('default/images/avatar.png'); ?>">
					<a href="<?php echo $this->_asset('default/images/avatar.png'); ?>?loaded"></a>
					<!-- <ul class="errorList">
						<li>Error message</li>
					</ul> -->
				</label>
			</div>

			<div class="grid-content-half f-right">
				<h4 class="heading-group">Endereço</h4>
				<label>CEP *<input name="cep" type="text" class="mask-postal" required></label>
				<label>Endereço *<input name="endereco" type="text" required></label>
				<label>Bairro *<input name="bairro" type="text" required></label>
				<label>Número *
					<input name="numero" type="text" required>
					<div class="note">Caso não possua, digite SN.</div>
				</label>
				<label>Complemento<input name="complemento" type="text"></label>
				<label>Cidade *<input name="cidade" type="text" required></label>
				<label>Estado *
					<select name="estado" required>
						<option value="AC">Acre</option>
						<option value="AL">Alagoas</option>
						<option value="AP">Amapá</option>
						<option value="AM">Amazonas</option>
						<option value="BA">Bahia</option>
						<option value="CE">Ceará</option>
						<option value="DF">Distrito Federal</option>
						<option value="ES">Espírito Santo</option>
						<option value="GO">Goiás</option>
						<option value="MA">Maranhão</option>
						<option value="MT">Mato Grosso</option>
						<option value="MS">Mato Grosso do Sul</option>
						<option value="MG">Minas Gerais</option>
						<option value="PA">Pará</option>
						<option value="PB">Paraíba</option>
						<option value="PR">Paraná</option>
						<option value="PE">Pernambuco</option>
						<option value="PI">Piauí</option>
						<option value="RJ">Rio de janeiro</option>
						<option value="RN">Rio Grande do Norte</option>
						<option value="RS">Rio Grande do Sul</option>
						<option value="RO">Rondônia</option>
						<option value="RR">Roraima</option>
						<option value="SC">Santa Catarina</option>
						<option value="SP">São Paulo</option>
						<option value="SE">Sergipe</option>
						<option value="TO">Tocantins</option>
					</select>
				</label>
			</div>
		</div>

		<div class="wrapper box-well box-well-highlight m-top-20">
			<div class="grid-content-half f-left a-right">
				<p>Digite sua <strong>senha atual</strong> no campo ao lado.</p>
				<p>Isto é necessário para comprovar a autenticidade das alterações.</p>
			</div>
			<div class="grid-content-half f-right">
				<input name="senha_atual" type="password" class="m-0" placeholder="Senha Atual" required>
			</div>
		</div>

		<div class="form-action">
			<button type="submit" class="button-submit">Confirmar Alteração</button>
		</div>
	</fieldset>
</form>
