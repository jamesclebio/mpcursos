<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Meu Extrato</h3>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<!-- <div class="content-empty">
	<p><strong>Nenhum lançamento foi realizado até o momento.</strong></p>
</div> -->

<table class="table-cart table-sort {sortlist: [[1, 1]]}">
	<thead>
		<tr>
			<th class="date">Data</th>
			<th class="description">Descrição</th>
			<th class="price">Valor</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="date">15/06/2013</td>
			<td class="description">Venda do vídeo D <strong>Lorem ipsum dolor sit amet consectetur adipisicing elit</strong> - <em>Nome do Curso Aqui</em></td>
			<td class="price">R$ 29,99</td>
		</tr>
		<tr>
			<td class="date">10/05/2013</td>
			<td class="description">Venda do vídeo C <strong>Lorem ipsum dolor sit amet consectetur adipisicing elit</strong> - <em>Nome do Curso Aqui</em></td>
			<td class="price">R$ 49,99</td>
		</tr>
		<tr class="line-success">
			<td class="date">25/08/2012</td>
			<td class="description"><strong>Pagamento MP Cursos</strong></td>
			<td class="price">R$ 69,99</td>
		</tr>
		<tr>
			<td class="date">13/07/2013</td>
			<td class="description">Venda do vídeo B <strong>Lorem ipsum dolor sit amet consectetur adipisicing elit</strong> - <em>Nome do Curso Aqui</em></td>
			<td class="price">R$ 89,99</td>
		</tr>
		<tr>
			<td class="date">11/06/2013</td>
			<td class="description">Venda do vídeo A <strong>Lorem ipsum dolor sit amet consectetur adipisicing elit</strong> - <em>Nome do Curso Aqui</em></td>
			<td class="price">R$ 99,99</td>
		</tr>
	</tbody>
</table>
