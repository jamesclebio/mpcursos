<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Meu Carrinho</h3>
<div class="linksbar">
	<ul>
		<li>Quantidade de itens no carrinho: <strong>0</strong></li>
		<li><a href="#">Limpar carrinho</a></li>
		<li><a href="#">Atualizar carrinho</a></li>
	</ul>
</div>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<!-- <div class="content-empty">
	<p><strong>Puxa, seu carrinho ainda está vazio... :(</strong></p>
	<p>Adicione produtos ao carrinho navegando pelo site através do menu principal localizado no topo da página.</p>
</div> -->

<form id="form-cart" method="post" action="" class="form-main">
	<fieldset>
		<legend>Carrinho</legend>
		<div class="form-action">
			<button type="button" class="button-large button-warning" onclick="window.location = '/';">Continuar comprando</button>
			<button type="submit" class="button-large button-success">Fechar Compra</button>
			<div class="wrapper m-top-15">
				<label class="check a-right">
					<input name="videotest" type="checkbox" value="1" required><strong>Efetuei os testes</strong> de funcionamento e li o manual do aluno no <a href="<?php echo $this->_url('video-test'); ?>" target="_blank" class="link-main">link de avaliação</a>. *
					<!-- <div>
						<ul class="errorList">
							<li>É necessário marcar esta opção continuar.</li>
						</ul>
					</div> -->
				</label>
			</div>
		</div>

		<table class="table-cart">
			<thead>
				<tr>
					<th class="description">Descrição</th>
					<th class="length">Tempo</th>
					<th class="length">Remover</th>
					<th class="price">Valor</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td class="description">
						<div class="item theme-course-1">
							<div class="header">
								<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
								<h5>Direito Constitucional</h5>
								<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
							</div>
						</div>
					</td>
					<td class="length"><div>Duração <strong>16h</strong></div></td>
					<td class="action"><a href="#">Remover</a></td>
					<td class="price">R$ 99,99</td>
				</tr>
				<tr>
					<td class="description">
						<div class="item theme-course-1">
							<div class="header">
								<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
								<h5>Direito Constitucional</h5>
								<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
							</div>
						</div>
					</td>
					<td class="length"><div>Duração <strong>16h</strong></div></td>
					<td class="action"><a href="#">Remover</a></td>
					<td class="price">R$ 99,99</td>
				</tr>
				<tr>
					<td class="description">
						<div class="item theme-course-1">
							<div class="header">
								<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
								<h5>Direito Constitucional</h5>
								<h4><a href="<?php echo $this->_url('product'); ?>">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
							</div>
						</div>
					</td>
					<td class="length"><div>Duração <strong>16h</strong></div></td>
					<td class="action"><a href="#">Remover</a></td>
					<td class="price">R$ 99,99</td>
				</tr>
			</tbody>
		</table>

		<div class="totalbar">Subtotal da compra: <strong>R$ 999,99</strong></div>

		<div class="discount">
			<p>Caso você tenha um <strong>Cupom de Desconto</strong>, informe o código do seu cupom no campo ao lado e clique no botão para aplicar o desconto:</p>
			<input name="desconto" type="text">
			<button type="button">Aplicar Desconto</button>
		</div>

		<div class="totalbar totalbar-highlight">Valor total da compra: <strong>R$ 999,99</strong></div>
		
		<label class="check a-right">
			<input name="videotest" type="checkbox" value="1" required><strong>Efetuei os testes</strong> de funcionamento e li o manual do aluno no <a href="<?php echo $this->_url('video-test'); ?>" target="_blank" class="link-main">link de avaliação</a>. *
			<!-- <div>
				<ul class="errorList">
					<li>É necessário marcar esta opção continuar.</li>
				</ul>
			</div> -->
		</label>

		<div class="form-action">
			<button type="button" class="button-large button-warning" onclick="window.location.href = '/';">Continuar comprando</button>
			<button type="submit" class="button-large button-success">Fechar Compra</button>
		</div>
	</fieldset>
</form>
