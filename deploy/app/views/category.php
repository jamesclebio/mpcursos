<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3 heading-star-yellow"><?php echo $this->product_category; ?></h3>

<!-- <div class="content-empty">
	<p><strong>Puxa, ainda não há cursos para a categoria selecionada... :(</strong></p>
	<p>Experimente usar nosso campo de busca mais acima ou navegue pelo site usando o menu principal localizado no topo da página.</p>
	<p>Se ainda assim não conseguir achar o que procura, <a href="mailto:cursos@livrorama.com.br" class="link-main">tente falar conosco</a>. Teremos prazer em ajudá-lo! ;D</p>
</div> -->

<div class="grid grid-items-4">
	<div class="grid-item">
		<nav class="nav-inside">
			<h3 class="heading-box heading-box-blue">Categorias</h3>
			<ul class="list-index">
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li class="current"><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
			</ul>
		</nav>

		<?php if ($this->product_category == 'Treinamentos'): ?>
			<div class="m-top-15">
				<a href="<?php echo $this->_url('trainings-conducted'); ?>" class="button-banner">Veja treinamentos realizados</a>
			</div>
		<?php endif; ?>
	</div>

	<div class="grid-item grid-item-span-3">
		<h3 class="heading-box">Todos os cursos</h3>

		<div class="product-showcase">
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current">R$ 99,99</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current price-free">Grátis</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current">R$ 99,99</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current">R$ 99,99</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current">R$ 99,99</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current">R$ 99,99</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current">R$ 99,99</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current">R$ 99,99</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current">R$ 99,99</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
		</div>

		<div class="pagination">
			<ul>
				<li class="current"><a href="#">1</a></li>
				<li><a href="#">2</a></li>
				<li><a href="#">3</a></li>
				<li><a href="#">4</a></li>
				<li><a href="#">5</a></li>
				<li><span>...</span></li>
				<li><a href="#">6</a></li>
				<li><a href="#">7</a></li>
				<li><a href="#">8</a></li>
				<li><a href="#">9</a></li>
				<li><a href="#">10</a></li>
			</ul>
		</div>
	</div>
</div>


