<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Autenticidade de diploma</h3>

<form id="form-certified" method="post" action="<?php echo $this->_url('certified/verify'); ?>" class="form-inline">
	<fieldset>
		<legend>Verificar certificado</legend>
		<input name="user" type="text" placeholder="Código do certificado" required>
		<button type="submit">Verificar</button>
	</fieldset>
</form>

<section class="section-certified">
	<div class="avatar"><img src="<?php echo $this->_asset('default/images/avatar.png'); ?>" alt=""></div>
	<div class="resume">
		<h1>James Clébio Silva de Assis</h1>
		<table class="table-list">
			<tbody>
				<tr>
					<th>Certificado</th>
					<td><strong>892364230874723</strong></td>
				</tr>
				<tr>
					<th>Curso</th>
					<td><strong>Lorem ipsum dolor sit amet, consectetur adipisicing elit</strong></td>
				</tr>
				<tr>
					<th>Key</th>
					<td>Value</td>
				</tr>
				<tr>
					<th>Key</th>
					<td>Value</td>
				</tr>
				<tr>
					<th>Key</th>
					<td>Value</td>
				</tr>
				<tr>
					<th>Key</th>
					<td>Value</td>
				</tr>
			</tbody>
		</table>
	</div>
</section>
