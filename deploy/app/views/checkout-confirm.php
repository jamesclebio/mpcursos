<h3 class="heading-3">Parabéns pela compra!</h3>

<div class="content-like">
	<p><strong>Sua compra foi realizada com sucesso! :)</strong></p>
	<p>Obrigado por comprar no MP Cursos!</p>
	<p>Logo logo as aulas adquiridas estarão disponíveis em sua página de cursos.</p>
</div>

<div class="wrapper group-separate group-separate-fine">
	<div class="f-right">
		<a href="<?php echo $this->_url('courses'); ?>" class="link-button link-button-large link-button-warning">Meus Cursos</a>
		<a href="<?php echo $this->_url('root'); ?>" class="link-button link-button-large link-button-success">Página Inicial</a>
	</div>
</div>
