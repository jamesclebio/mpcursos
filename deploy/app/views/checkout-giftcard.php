<h4 class="heading-4 heading-separate">Cartão presente</h4>

<?php if ($this->success): ?>
	<div class="alert-main alert-main-success">
		<p><strong>Parabéns! O curso Lorem Ipsum foi adicionado com sucesso à sua conta.</strong></p>
		<p>Para acessá-lo, basta ir à <a href="<?php echo $this->_url('sign/up'); ?>">página de cursos</a> de nosso portal.</p>
	</div>
<?php else: ?>
	<!-- <div class="alert-main alert-main-info">
		<p><strong>Você precisa estar logado para usar o cartão presente!</strong></p>
		<p>Use o formulário Área do Aluno no topo do site para logar.</p>
		<p>Caso ainda não possua usuário e senha, <a href="<?php echo $this->_url('sign/up'); ?>">cadastre-se aqui</a>.</p>
	</div> -->

	<form id="form-giftcard" method="post" action="<?php echo $this->_url('checkout/giftcard/check/2'); ?>" class="form-inline">
		<fieldset>
			<legend>Cartão presente</legend>
			<input name="card" type="text" placeholder="Código do cartão" required>
			<button type="submit">Confirmar</button>
			<div class="alert"></div>
		</fieldset>
	</form>
<?php endif; ?>
