<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Fale Conosco</h3>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Sua mensagem foi enviada com sucesso!</strong></p>
	<p>Daremos uma resposta o mais breve possível. ;D</p>
</div> -->

<p>Você pode falar conosco enviando uma mensagem para <a href="mailto:sac@mpcursos.com" class="link-main">sac@mpcursos.com</a> ou usando o formulário abaixo.</p>

<!-- Form -->
<form id="form-contact" method="post" action="" class="form-main group-separate">
	<fieldset>
		<legend>Contato</legend>
		<div class="wrapper">
			<div class="grid-content-half f-left">
				<h4 class="heading-group">Seus Dados</h4>
				<label>Nome *<input name="nome" type="text" required></label>
				<label>E-mail *<input name="email" type="email" required></label>
				<label>Telefone<input name="telefone" type="text" class="mask-phone"></label>
			</div>

			<div class="grid-content-half f-right">
				<h4 class="heading-group">Sua Mensagem</h4>
				<label>Mensagem *<textarea name="mensagem" id="" cols="30" rows="10" class="h-190"></textarea></label>
			</div>
		</div>

		<div class="form-action">
			<button type="submit" class="button-submit">Enviar Agora</button>
		</div>
	</fieldset>
</form>
