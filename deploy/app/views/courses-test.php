<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Avaliação</h3>

<section class="section-test">
	<header>
		<h1>Curso Lorem ipsum dolor sit amet, consectetur adipisicing elit</h1>
	</header>

	<!-- <div class="note note-info">
		<h2>Resultado</h2>
		<p>Avaliação respondida em: <strong>19/19/1919 19:19:19</strong></p>
		<p>Número de questões: <strong>10</strong> (<strong>7</strong> acertos, <strong>3</strong> erros)</p>
		<p>Nota obtida: <strong>7</strong> pontos</p>
	</div> -->

	<form id="form-test" method="post" action="" class="form-main">
		<fieldset>
			<legend>Avaliação</legend>

			<div class="block-question">
				<h5><span>1.</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, libero?</h5>
				<div class="answer">
					<label class="check"><input name="q1" type="radio" required>Lorem ipsum dolor sit amet</label>
					<label class="check"><input name="q1" type="radio">Lorem ipsum dolor sit amet</label>
					<label class="check"><input name="q1" type="radio">Lorem ipsum dolor sit amet</label>
					<label class="check"><input name="q1" type="radio">Lorem ipsum dolor sit amet</label>
				</div>
			</div>
			<div class="block-question">
				<h5><span>2.</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, libero?</h5>
				<div class="answer">
					<label class="check"><input name="q2" type="radio" required>Lorem ipsum dolor sit amet</label>
					<label class="check"><input name="q2" type="radio">Lorem ipsum dolor sit amet</label>
					<label class="check"><input name="q2" type="radio">Lorem ipsum dolor sit amet</label>
					<label class="check"><input name="q2" type="radio">Lorem ipsum dolor sit amet</label>
				</div>
			</div>
			<div class="block-question">
				<h5><span>3.</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, libero?</h5>
				<div class="answer">
					<label class="check"><input name="q3" type="radio" required>Lorem ipsum dolor sit amet</label>
					<label class="check"><input name="q3" type="radio">Lorem ipsum dolor sit amet</label>
					<label class="check"><input name="q3" type="radio">Lorem ipsum dolor sit amet</label>
					<label class="check"><input name="q3" type="radio">Lorem ipsum dolor sit amet</label>
				</div>
			</div>

			<div class="form-action">
				<div class="note">
					<h2>Confirmação das respostas</h2>
					<label class="check">
						<p><input type="checkbox" name="confirm" required><strong>Estou ciente de que ao clicar em "confirmar respostas" não será permitido alterar as repostas escolhidas.</strong></p>
						<p>Desta forma, o diploma não será emitido caso a nota final do aluno seja inferior à minima necessária para aprovação no curso.</p>
						<p>Em caso de reprovação, a prova só poderá ser refeita caso o aluno se inscreva em um novo curso.</p>
					</label>
				</div>

				<button type="submit" class="button-large button-submit">Confirmar respostas</button>
			</div>
		</fieldset>
	</form>
</section>
