<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Perguntas e Respostas</h3>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="wrapper m-top-30" id="faq-1">
	<h5 class="heading-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit asperiores itaque nam?</h5>
	<div class="text-body">
		<p>Lorem ipsum dolor sit amet, consectetur <a href="#">adipisicing elit</a>. Soluta, culpa quo laboriosam totam! Vitae, explicabo, neque repudiandae aliquid vero veniam maxime numquam incidunt laboriosam magnam voluptas quam itaque est enim possimus debitis alias molestiae laborum aut iure velit quidem eligendi optio libero asperiores at repellat sequi? Voluptates, fugiat, nesciunt, perferendis mollitia ullam molestiae ut placeat amet soluta eveniet exercitationem id porro iste pariatur itaque magni aliquam quam ex corrupti facilis possimus quidem temporibus reprehenderit cupiditate excepturi ipsum ad est optio doloribus. Odio, reiciendis, minima totam voluptatum suscipit magni atque perspiciatis quo dicta at itaque blanditiis nobis dignissimos quas praesentium nulla.</p>
	</div>
</div>
<div class="wrapper m-top-30" id="faq-2">
	<h5 class="heading-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit asperiores itaque nam?</h5>
	<div class="text-body">
		<p>Lorem ipsum dolor sit amet, consectetur <a href="#">adipisicing elit</a>. Soluta, culpa quo laboriosam totam! Vitae, explicabo, neque repudiandae aliquid vero veniam maxime numquam incidunt laboriosam magnam voluptas quam itaque est enim possimus debitis alias molestiae laborum aut iure velit quidem eligendi optio libero asperiores at repellat sequi? Voluptates, fugiat, nesciunt, perferendis mollitia ullam molestiae ut placeat amet soluta eveniet exercitationem id porro iste pariatur itaque magni aliquam quam ex corrupti facilis possimus quidem temporibus reprehenderit cupiditate excepturi ipsum ad est optio doloribus. Odio, reiciendis, minima totam voluptatum suscipit magni atque perspiciatis quo dicta at itaque blanditiis nobis dignissimos quas praesentium nulla.</p>
	</div>
</div>
<div class="wrapper m-top-30" id="faq-3">
	<h5 class="heading-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit asperiores itaque nam?</h5>
	<div class="text-body">
		<p>Lorem ipsum dolor sit amet, consectetur <a href="#">adipisicing elit</a>. Soluta, culpa quo laboriosam totam! Vitae, explicabo, neque repudiandae aliquid vero veniam maxime numquam incidunt laboriosam magnam voluptas quam itaque est enim possimus debitis alias molestiae laborum aut iure velit quidem eligendi optio libero asperiores at repellat sequi? Voluptates, fugiat, nesciunt, perferendis mollitia ullam molestiae ut placeat amet soluta eveniet exercitationem id porro iste pariatur itaque magni aliquam quam ex corrupti facilis possimus quidem temporibus reprehenderit cupiditate excepturi ipsum ad est optio doloribus. Odio, reiciendis, minima totam voluptatum suscipit magni atque perspiciatis quo dicta at itaque blanditiis nobis dignissimos quas praesentium nulla.</p>
	</div>
</div>

<!-- Guide Steps -->
<div class="guidesteps">
	<h2>Em apenas 3 passos você compra seu curso. Veja como é simples:</h2>
	<div class="step step-1">
		<h3><span>01</span> Como se cadastrar</h3>
		<div class="content">
			<p>Para se cadastrar basta clicar no link <a href="<?php echo $this->_url('sign-up'); ?>">Cadastre-se!</a> localizado na barra de Área do Aluno. Em seguida é necessário preencher o formulário de cadastro e em poucos minutos você estará cadastrado.</p>
		</div>
	</div>
	<div class="step step-2">
		<h3><span>02</span> Como comprar um curso</h3>
		<div class="content">
			<p>Para comprar um curso é necessário que você esteja previamente cadastrado. Comprar um curso é fácil, basta escolher o curso desejado e clicar no botão comprar. Você será direcionado para o carrinho de compras onde deverá confirmar a sua compra.</p>
			<p>Não esqueça de <a href="<?php echo $this->_url('video-test'); ?>">efetuar os testes de funcionamento</a> para garantir sua perfeita visualização do curso.</p>
		</div>
	</div>
	<div class="step step-3">
		<h3><span>03</span> Como assistir a um curso comprado</h3>
		<div class="content">
			<p>Para assistir, acesse a "ÁREA DO ALUNO" localizado na barra superior do portal. Após digitar seu "USUÁRIO e SENHA", você deverá ir em "MINHA CONTA" e clicar em <a href="<?php echo $this->_url('courses'); ?>">Meus Cursos</a>.</p>
			<p>Pronto, serão exibidos todos os cursos que você comprou. Basta clicar no curso desejado.</p>
		</div>
	</div>
</div>