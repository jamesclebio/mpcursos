<div class="grid grid-items-4">
	<div class="grid-item">
		<nav class="nav-inside">
			<h3 class="heading-box heading-box-darkblue">Nossos cursos</h3>
			<ul class="list-index">
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
				<li><a href="#">Lorem ipsum dolor</a></li>
			</ul>
		</nav>
	</div>

	<div class="grid-item grid-item-span-3">
		<h3 class="heading-box">Cursos em destaque</h3>
		<div class="product-showcase">
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current">R$ 99,99</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current price-free">Grátis</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current">R$ 99,99</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current">R$ 99,99</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current">R$ 99,99</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current">R$ 99,99</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
		</div>
	</div>
</div>

<div class="m-top-15">
	<a href="#"><img src="<?php echo $this->_asset('default/images/banners/projeto-saber.jpg'); ?>" alt="Promoção Compartilhe e Ganhe"></a>
</div>

<div class="grid grid-items-4 m-top-15">
	<div class="grid-item">
		<h3 class="heading-box heading-box-darkyellow">Agenda de treinamento</h3>
		<div class="calendar">
			<ul>
				<li>
					<a href="<?php echo $this->_url('product'); ?>">
						<div class="date">
							<div class="day">25</div>
							<div class="month">mar</div>
						</div>
						<div class="resume">
							<div class="heading">Lorem ipsum dolor</div>
							<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit cumque...</div>
						</div>
					</a>
				</li>
				<li>
					<a href="<?php echo $this->_url('product'); ?>">
						<div class="date">
							<div class="day">25</div>
							<div class="month">mar</div>
						</div>
						<div class="resume">
							<div class="heading">Lorem ipsum dolor</div>
							<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit cumque...</div>
						</div>
					</a>
				</li>
				<li>
					<a href="<?php echo $this->_url('product'); ?>">
						<div class="date">
							<div class="day">25</div>
							<div class="month">mar</div>
						</div>
						<div class="resume">
							<div class="heading">Lorem ipsum dolor</div>
							<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit cumque...</div>
						</div>
					</a>
				</li>
			</ul>
			<a href="<?php echo $this->_url('product/trainings'); ?>" class="all">Ver todos</a>
		</div>

		<div class="testimonials">
			<h4>Depoimento</h4>
			<blockquote>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta, quos repellat obcaecati molestiae quasi saepe ipsum voluptatum architecto mollitia sint? Labore, provident, omnis cupiditate impedit.</p>
				<footer>Saulo Duarte</footer>
			</blockquote>
		</div>
	</div>

	<div class="grid-item grid-item-span-3">
		<h3 class="heading-box heading-box-blue">Notícias</h3>
		<div class="slider-news">
			<ul class="items">
				<li>
					<a href="<?php echo $this->_url('news/view'); ?>">
						<img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt="">
						<h4>1 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, doloribus</h4>
						<div class="date">Sábado, 12 de maio de 2014</div>
						<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis rem facere voluptate neque dolores praesentium dolore aperiam doloremque! Alias, quisquam pariatur consequatur itaque ratione nobis quis. Ipsa, non animi voluptas.</div>
					</a>
				</li>
				<li>
					<a href="<?php echo $this->_url('news/view'); ?>">
						<img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt="">
						<h4>2 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, doloribus</h4>
						<div class="date">Sábado, 12 de maio de 2014</div>
						<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis rem facere voluptate neque dolores praesentium dolore aperiam doloremque! Alias, quisquam pariatur consequatur itaque ratione nobis quis. Ipsa, non animi voluptas.</div>
					</a>
				</li>
				<li>
					<a href="<?php echo $this->_url('news/view'); ?>">
						<img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt="">
						<h4>3 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, doloribus</h4>
						<div class="date">Sábado, 12 de maio de 2014</div>
						<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis rem facere voluptate neque dolores praesentium dolore aperiam doloremque! Alias, quisquam pariatur consequatur itaque ratione nobis quis. Ipsa, non animi voluptas.</div>
					</a>
				</li>
			</ul>
		</div>

		<h3 class="heading-box heading-box-darkblue">Cursos mais vendidos</h3>
		<div class="product-showcase">
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current">R$ 99,99</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current price-free">Grátis</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
			<div class="product-item">
				<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
				<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
				<div class="price-old">R$ 99,99</div>
				<div class="price-current">R$ 99,99</div>
				<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
			</div>
		</div>
	</div>
</div>
