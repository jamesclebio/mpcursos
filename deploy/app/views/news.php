<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Notícias</h3>

<?php if ($this->news_view): ?>
	<section class="section-post">
		<h1 class="heading">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat, maxime vero perferendis. Possimus, consequatur, et.</h1>
		<div class="date">Sábado, 10 de maio de 2014</div>
		<div class="text-body">
			<p>Lorem ipsum dolor sit amet, <a href="#">consectetur</a> adipisicing elit. Quo, culpa illum cupiditate nam fugit mollitia reprehenderit commodi nihil ad soluta vero tenetur rem error voluptatum hic corrupti accusantium quis minus sed ducimus perferendis? Ut, earum doloremque laborum libero fuga est vitae cum voluptate porro sequi culpa cumque amet accusamus repellat quidem in magnam voluptatibus! Animi, ratione tempore impedit accusamus fuga.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorum, ea, itaque, possimus, ullam ut perspiciatis culpa corrupti eum reprehenderit labore laboriosam accusamus impedit nam laborum distinctio dolores sed repudiandae libero illum sit. Numquam non a voluptate consequatur ea? Odit, facere!</p>
		</div>
	</section>
<?php else: ?>
	<h4 class="heading-box heading-box-blue">Destaques</h4>
	<div class="slider-news">
		<ul class="items">
			<li>
				<a href="<?php echo $this->_url('news/view'); ?>">
					<img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt="">
					<h4>1 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, doloribus</h4>
					<div class="date">Sábado, 12 de maio de 2014</div>
					<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis rem facere voluptate neque dolores praesentium dolore aperiam doloremque! Alias, quisquam pariatur consequatur itaque ratione nobis quis. Ipsa, non animi voluptas.</div>
				</a>
			</li>
			<li>
				<a href="<?php echo $this->_url('news/view'); ?>">
					<img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt="">
					<h4>2 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, doloribus</h4>
					<div class="date">Sábado, 12 de maio de 2014</div>
					<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis rem facere voluptate neque dolores praesentium dolore aperiam doloremque! Alias, quisquam pariatur consequatur itaque ratione nobis quis. Ipsa, non animi voluptas.</div>
				</a>
			</li>
			<li>
				<a href="<?php echo $this->_url('news/view'); ?>">
					<img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt="">
					<h4>3 Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias, doloribus</h4>
					<div class="date">Sábado, 12 de maio de 2014</div>
					<div class="resume">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis rem facere voluptate neque dolores praesentium dolore aperiam doloremque! Alias, quisquam pariatur consequatur itaque ratione nobis quis. Ipsa, non animi voluptas.</div>
				</a>
			</li>
		</ul>
	</div>
<?php endif; ?>

<div class="m-top-15">
	<h4 class="heading-box heading-box-darkyellow">Outras notícias</h4>

	<ul class="list-index">
		<li><a href="<?php echo $this->_url('news/view'); ?>"><span class="date">10/10/1010</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, quibusdam mollitia quae cum praesentium optio.</a></li>
		<li><a href="<?php echo $this->_url('news/view'); ?>"><span class="date">10/10/1010</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, quibusdam mollitia quae cum praesentium optio.</a></li>
		<li><a href="<?php echo $this->_url('news/view'); ?>"><span class="date">10/10/1010</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, quibusdam mollitia quae cum praesentium optio.</a></li>
		<li><a href="<?php echo $this->_url('news/view'); ?>"><span class="date">10/10/1010</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, quibusdam mollitia quae cum praesentium optio.</a></li>
		<li><a href="<?php echo $this->_url('news/view'); ?>"><span class="date">10/10/1010</span> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus, quibusdam mollitia quae cum praesentium optio.</a></li>
	</ul>

	<div class="pagination">
		<ul>
			<li class="current"><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li><span>...</span></li>
			<li><a href="#">6</a></li>
			<li><a href="#">7</a></li>
			<li><a href="#">8</a></li>
			<li><a href="#">9</a></li>
			<li><a href="#">10</a></li>
		</ul>
	</div>
</div>
