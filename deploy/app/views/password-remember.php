<h4 class="heading-4 heading-separate">Lembrar Senha</h4>
<form id="form-password" method="post" action="<?php echo $this->_url('password/reset'); ?>" class="form-inline">
	<fieldset>
		<legend>Lembrar senha</legend>
		<input name="user" type="text" placeholder="Usuário" required>
		<button type="submit">Lembrar</button>
		<div class="alert"></div>
	</fieldset>
</form>