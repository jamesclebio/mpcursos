<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<div class="details-course">
	<div class="item theme-course-1">
		<div class="header">
			<img src="<?php echo $this->_asset('default/images/thumb_course.png'); ?>" alt="">
			<h5>Direito Constitucional</h5>
			<h4>Lorem ipsum dolor sit amet consectetur adipisicing elit</h4>
		</div>
		<div class="resume">
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo officiis praesentium sapiente unde aut rem tempora vero libero atque mollitia.</p>
		</div>
		<div class="details">
			<div class="price">R$ <strong>699,90</strong></div>
			<!-- <div class="length">Duração <strong>16h</strong></div> -->
			<div class="length">12 vagas disponíveis</div>
		</div>
		<!-- <div class="note">* Vídeo aula + Material de apoio + Links</div> -->
		<ul class="action">
			<li class="buy"><a href="<?php echo $this->_url('cart'); ?>">Comprar</a></li>
		</ul>
	</div>
	<div class="preview">
		<object id="video-player" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="510" height="287">
			<param name="movie" value="<?php echo $this->_asset('default/movies/player/video_player8_xml.swf'); ?>">
			<param name="allowFullScreen" value="true">
			<!--[if !IE]>-->
			<object type="application/x-shockwave-flash" data="<?php echo $this->_asset('default/movies/player/video_player8_xml.swf'); ?>" width="510" height="287" allowfullscreen="true">
			<!--<![endif]-->
			<div class="no-videoplayer">
				<h1>Ops, vídeo não suportado! :(</h1>
				<p>Aparentemente seu navegador não possui os recursos básicos para rodar nosso player de vídeo.</p>
				<p>Atualize o Flash Player e tente novamente:</p>
				<p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
			</div>
			<!--[if !IE]>-->
			</object>
			<!--<![endif]-->
		</object>
	</div>
</div>

<div class="m-top-15">
	<div class="collapse-main collapse-main-open">
		<a href="#" class="collapse-main-heading"><span>+</span>Sobre o Curso</a>
		<div class="collapse-main-content">
			<div class="text-body">
				<h1>Heading h1</h1>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dicta, harum, enim, vel doloremque porro hic doloribus architecto vero quaerat sint sequi et neque id qui placeat fugit laborum itaque! Sint.</p>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Labore, praesentium nemo cupiditate odio voluptatum et dignissimos sint aliquam vero soluta id mollitia suscipit? Consequuntur, consequatur commodi temporibus nesciunt iure officiis.</p>
				<h2>Heading h2</h2>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, ratione, voluptatibus, ab, saepe autem temporibus ut nihil optio sed culpa assumenda qui unde eaque repudiandae sint eius distinctio earum doloribus aperiam dolore deserunt minus voluptas laborum voluptatem ipsum omnis itaque? Est, quo officia nobis aliquid sit delectus. Hic, inventore, sed!</p>
				<h3>Heading h3</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, ratione, voluptatibus, ab, saepe autem temporibus ut nihil optio sed culpa assumenda qui unde eaque repudiandae sint eius distinctio earum doloribus aperiam dolore deserunt minus voluptas laborum voluptatem ipsum omnis itaque? Est, quo officia nobis aliquid sit delectus. Hic, inventore, sed!</p>
				<h4>Heading h4</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, ratione, voluptatibus, ab, saepe autem temporibus ut nihil optio sed culpa assumenda qui unde eaque repudiandae sint eius distinctio earum doloribus aperiam dolore deserunt minus voluptas laborum voluptatem ipsum omnis itaque? Est, quo officia nobis aliquid sit delectus. Hic, inventore, sed!</p>
				<h5>Heading h5</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Commodi, ratione, voluptatibus, ab, saepe autem temporibus ut nihil optio sed culpa assumenda qui unde eaque repudiandae sint eius distinctio earum doloribus aperiam dolore deserunt minus voluptas laborum voluptatem ipsum omnis itaque? Est, quo officia nobis aliquid sit delectus. Hic, inventore, sed!</p>
				<ul>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Enim, inventore.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum, explicabo.</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure, excepturi?</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eius, perspiciatis.</li>
				</ul>
			</div>
		</div>
	</div>

	<div class="collapse-main">
		<a href="#" class="collapse-main-heading"><span>+</span>Perguntas e Respostas</a>
		<div class="collapse-main-content">
			<div class="wrapper m-top-30" id="faq-1">
				<h5 class="heading-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit asperiores itaque nam?</h5>
				<div class="text-body">
					<p>Lorem ipsum dolor sit amet, consectetur <a href="#">adipisicing elit</a>. Soluta, culpa quo laboriosam totam! Vitae, explicabo, neque repudiandae aliquid vero veniam maxime numquam incidunt laboriosam magnam voluptas quam itaque est enim possimus debitis alias molestiae laborum aut iure velit quidem eligendi optio libero asperiores at repellat sequi? Voluptates, fugiat, nesciunt, perferendis mollitia ullam molestiae ut placeat amet soluta eveniet exercitationem id porro iste pariatur itaque magni aliquam quam ex corrupti facilis possimus quidem temporibus reprehenderit cupiditate excepturi ipsum ad est optio doloribus. Odio, reiciendis, minima totam voluptatum suscipit magni atque perspiciatis quo dicta at itaque blanditiis nobis dignissimos quas praesentium nulla.</p>
				</div>
			</div>
			<div class="wrapper m-top-30" id="faq-2">
				<h5 class="heading-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit asperiores itaque nam?</h5>
				<div class="text-body">
					<p>Lorem ipsum dolor sit amet, consectetur <a href="#">adipisicing elit</a>. Soluta, culpa quo laboriosam totam! Vitae, explicabo, neque repudiandae aliquid vero veniam maxime numquam incidunt laboriosam magnam voluptas quam itaque est enim possimus debitis alias molestiae laborum aut iure velit quidem eligendi optio libero asperiores at repellat sequi? Voluptates, fugiat, nesciunt, perferendis mollitia ullam molestiae ut placeat amet soluta eveniet exercitationem id porro iste pariatur itaque magni aliquam quam ex corrupti facilis possimus quidem temporibus reprehenderit cupiditate excepturi ipsum ad est optio doloribus. Odio, reiciendis, minima totam voluptatum suscipit magni atque perspiciatis quo dicta at itaque blanditiis nobis dignissimos quas praesentium nulla.</p>
				</div>
			</div>
			<div class="wrapper m-top-30" id="faq-3">
				<h5 class="heading-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit asperiores itaque nam?</h5>
				<div class="text-body">
					<p>Lorem ipsum dolor sit amet, consectetur <a href="#">adipisicing elit</a>. Soluta, culpa quo laboriosam totam! Vitae, explicabo, neque repudiandae aliquid vero veniam maxime numquam incidunt laboriosam magnam voluptas quam itaque est enim possimus debitis alias molestiae laborum aut iure velit quidem eligendi optio libero asperiores at repellat sequi? Voluptates, fugiat, nesciunt, perferendis mollitia ullam molestiae ut placeat amet soluta eveniet exercitationem id porro iste pariatur itaque magni aliquam quam ex corrupti facilis possimus quidem temporibus reprehenderit cupiditate excepturi ipsum ad est optio doloribus. Odio, reiciendis, minima totam voluptatum suscipit magni atque perspiciatis quo dicta at itaque blanditiis nobis dignissimos quas praesentium nulla.</p>
				</div>
			</div>
			<div class="wrapper m-top-30" id="faq-4">
				<h5 class="heading-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit asperiores itaque nam?</h5>
				<div class="text-body">
					<p>Lorem ipsum dolor sit amet, consectetur <a href="#">adipisicing elit</a>. Soluta, culpa quo laboriosam totam! Vitae, explicabo, neque repudiandae aliquid vero veniam maxime numquam incidunt laboriosam magnam voluptas quam itaque est enim possimus debitis alias molestiae laborum aut iure velit quidem eligendi optio libero asperiores at repellat sequi? Voluptates, fugiat, nesciunt, perferendis mollitia ullam molestiae ut placeat amet soluta eveniet exercitationem id porro iste pariatur itaque magni aliquam quam ex corrupti facilis possimus quidem temporibus reprehenderit cupiditate excepturi ipsum ad est optio doloribus. Odio, reiciendis, minima totam voluptatum suscipit magni atque perspiciatis quo dicta at itaque blanditiis nobis dignissimos quas praesentium nulla.</p>
				</div>
			</div>
			<div class="wrapper m-top-30" id="faq-5">
				<h5 class="heading-4">Lorem ipsum dolor sit amet, consectetur adipisicing elit asperiores itaque nam?</h5>
				<div class="text-body">
					<p>Lorem ipsum dolor sit amet, consectetur <a href="#">adipisicing elit</a>. Soluta, culpa quo laboriosam totam! Vitae, explicabo, neque repudiandae aliquid vero veniam maxime numquam incidunt laboriosam magnam voluptas quam itaque est enim possimus debitis alias molestiae laborum aut iure velit quidem eligendi optio libero asperiores at repellat sequi? Voluptates, fugiat, nesciunt, perferendis mollitia ullam molestiae ut placeat amet soluta eveniet exercitationem id porro iste pariatur itaque magni aliquam quam ex corrupti facilis possimus quidem temporibus reprehenderit cupiditate excepturi ipsum ad est optio doloribus. Odio, reiciendis, minima totam voluptatum suscipit magni atque perspiciatis quo dicta at itaque blanditiis nobis dignissimos quas praesentium nulla.</p>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="mt-top-15">
	<h3 class="heading-box">Cursos em destaque</h3>
	<div class="product-showcase">
		<div class="product-item">
			<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
			<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
			<div class="price-old">R$ 99,99</div>
			<div class="price-current">R$ 99,99</div>
			<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
		</div>
		<div class="product-item">
			<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
			<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
			<div class="price-old">R$ 99,99</div>
			<div class="price-current price-free">Grátis</div>
			<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
		</div>
		<div class="product-item">
			<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
			<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
			<div class="price-old">R$ 99,99</div>
			<div class="price-current">R$ 99,99</div>
			<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
		</div>
		<div class="product-item">
			<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
			<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
			<div class="price-old">R$ 99,99</div>
			<div class="price-current">R$ 99,99</div>
			<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
		</div>
	</div>
</div>

<div class="m-top-15">
	<a href="#"><img src="<?php echo $this->_asset('default/images/banners/projeto-saber.jpg'); ?>" alt="Promoção Compartilhe e Ganhe"></a>
</div>
