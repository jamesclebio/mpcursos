<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<!-- Resultado -->
<div class="wrapper">
	<h3 class="heading-3 heading-star-yellow">Buscando por <span>termo pesquisado</span></h3>
	<!-- <div class="content-empty">
		<p><strong>Desculpe, sua busca não retornou nenhum resultado. :(</strong></p>
		<p>Tente fazer uma nova busca ou navegue pelo site usando o menu principal localizado no topo da página.</p>
	</div> -->

	<div class="product-showcase">
		<div class="product-item">
			<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
			<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
			<div class="price-old">R$ 99,99</div>
			<div class="price-current">R$ 99,99</div>
			<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
		</div>
		<div class="product-item">
			<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
			<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
			<div class="price-old">R$ 99,99</div>
			<div class="price-current price-free">Grátis</div>
			<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
		</div>
		<div class="product-item">
			<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
			<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
			<div class="price-old">R$ 99,99</div>
			<div class="price-current">R$ 99,99</div>
			<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
		</div>
		<div class="product-item">
			<div class="image"><a href="<?php echo $this->_url('product'); ?>"><img src="<?php echo $this->_asset('default/images/tractor.jpg'); ?>" alt=""></a></div>
			<div class="description">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Id, ipsa quam officiis esse inventore rerum</div>
			<div class="price-old">R$ 99,99</div>
			<div class="price-current">R$ 99,99</div>
			<div class="buy"><a href="<?php echo $this->_url('product'); ?>">Comprar</a></div>
		</div>
	</div>

	<div class="pagination">
		<ul>
			<li class="current"><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">4</a></li>
			<li><a href="#">5</a></li>
			<li><span>...</span></li>
			<li><a href="#">6</a></li>
			<li><a href="#">7</a></li>
			<li><a href="#">8</a></li>
			<li><a href="#">9</a></li>
			<li><a href="#">10</a></li>
		</ul>
	</div>
</div>
