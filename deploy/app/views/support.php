<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Material de Apoio</h3>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<!-- <div class="content-empty">
	<p><strong>Você ainda não possui nenhum material cadastrado para o seus vídeos.</strong></p>
	<p>Use o formulário abaixo para adicionar novos arquivos.</p>
</div> -->

<table class="table-cart">
	<thead>
		<tr>
			<th class="title">Arquivo</th>
			<th class="list">Vídeo(s)</th>
			<th class="action"></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td class="title">
				<h4><a href="#" target="_blank">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				<div class="details">
					PDF, 345Kb<br>
					Adicionado em 99/99/9999, às 15:15:15
				</div>
			</td>
			<td class="list">
				<ul>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, doloribus natus commodi ab possimus alias!</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad doloribus similique enim provident harum?</li>
				</ul>
			</td>
			<td class="action"><a href="<?php echo $this->_url('support/edit'); ?>">Editar</a> | <a href="#">Remover</a></td>
		</tr>
		<tr>
			<td class="title">
				<h4><a href="#" target="_blank">Lorem ipsum dolor sit amet consectetur adipisicing elit</a></h4>
				<div class="details">
					PDF, 345Kb<br>
					Adicionado em 99/99/9999, às 15:15:15
				</div>
			</td>
			<td class="list">
				<ul>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa, doloribus natus commodi ab possimus alias!</li>
					<li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab ad doloribus similique enim provident harum?</li>
				</ul>
			</td>
			<td class="action"><a href="<?php echo $this->_url('support/edit'); ?>">Editar</a> | <a href="#">Remover</a></td>
		</tr>
	</tbody>
</table>

<h4 class="heading-group">Novo Arquivo</h4>
<div class="box-well">
	<form id="form-support" action="" class="form-main">
		<fieldset>
			<legend>Suporte</legend>
			<div class="clearfix">
				<div class="w-450 f-left">
					<label>Título *<input name="titulo" type="text" required></label>
					<label>Arquivo *
						<input name="arquivo" type="file" required>
						<div class="note">Tamanho máximo: <strong>999</strong> megabytes.</div>
					</label>
				</div>
				<div class="w-450 f-right">
					<label>Vídeos
						<select name="videos" multiple data-placeholder="Clique para selecionar os vídeos" class="chosen">
							<option value="1">Lorem ipsum dolor sit amet, consectetur sdgh ghs hsgdsd shdgsdsds adipisicing elit nost</option>
							<option value="2">Lorem ipsum dolor sit amet, consectetur adipisicing elit nost</option>
							<option value="3">Lorem ipsum dolor sit amet, consectetur adipisicing elit nost</option>
						</select>
					</label>
				</div>
			</div>
			<div class="form-action">
				<button type="submit" class="button-submit">Adicionar Agora</button>
			</div>
		</fieldset>
	</form>
</div>
