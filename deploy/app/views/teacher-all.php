<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Nossos Professores</h3>
<p>Clique no professor para ver mais:</p>

<div class="wrapper m-top-30">
	<ul class="list-avatar list-avatar-grid">
		<li>
			<a href="<?php echo $this->_url('teacher/profile'); ?>">
				<div class="thumb"><img src="<?php echo $this->_asset('default/images/avatar.png'); ?>" alt="Avatar"></div>
				<h5>Saulo Duarte e Couto</h5>
				<p>Juíza do Trabalho. Professora Adjunta da Universidade Federal de Sergipe. Coordenadora da Escola Judicial do Tribunal Regional do Trabalho da 20 Região. Diretora da Escola da Associação dos Magistrados da Justiça do Trabalho da 20 Região.  Diretora Cultural da AMATRA XX. Doutora em Direito Público. Mestre em Direito, Estado e Cidadania. Especialista em Direito Processual.</p>
			</a>
		</li>
		<li>
			<a href="<?php echo $this->_url('teacher/profile'); ?>">
				<div class="thumb"><img src="<?php echo $this->_asset('default/images/avatar.png'); ?>" alt="Avatar"></div>
				<h5>Saulo Duarte e Couto</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem dolor consectetur veritatis distinctio repellendus animi beatae dolorem facere aperiam laudantium.</p>
			</a>
		</li>
		<li>
			<a href="<?php echo $this->_url('teacher/profile'); ?>">
				<div class="thumb"><img src="<?php echo $this->_asset('default/images/avatar.png'); ?>" alt="Avatar"></div>
				<h5>Saulo Duarte e Couto</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem dolor consectetur veritatis distinctio repellendus animi beatae dolorem facere aperiam laudantium.</p>
			</a>
		</li>
		<li>
			<a href="<?php echo $this->_url('teacher/profile'); ?>">
				<div class="thumb"><img src="<?php echo $this->_asset('default/images/avatar.png'); ?>" alt="Avatar"></div>
				<h5>Saulo Duarte e Couto</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem dolor consectetur veritatis distinctio repellendus animi beatae dolorem facere aperiam laudantium.</p>
			</a>
		</li>
		<li>
			<a href="<?php echo $this->_url('teacher/profile'); ?>">
				<div class="thumb"><img src="<?php echo $this->_asset('default/images/avatar.png'); ?>" alt="Avatar"></div>
				<h5>Saulo Duarte e Couto</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem dolor consectetur veritatis distinctio repellendus animi beatae dolorem facere aperiam laudantium.</p>
			</a>
		</li>
		<li>
			<a href="<?php echo $this->_url('teacher/profile'); ?>">
				<div class="thumb"><img src="<?php echo $this->_asset('default/images/avatar.png'); ?>" alt="Avatar"></div>
				<h5>Saulo Duarte e Couto</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem dolor consectetur veritatis distinctio repellendus animi beatae dolorem facere aperiam laudantium.</p>
			</a>
		</li>
		<li>
			<a href="<?php echo $this->_url('teacher/profile'); ?>">
				<div class="thumb"><img src="<?php echo $this->_asset('default/images/avatar.png'); ?>" alt="Avatar"></div>
				<h5>Saulo Duarte e Couto</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem dolor consectetur veritatis distinctio repellendus animi beatae dolorem facere aperiam laudantium.</p>
			</a>
		</li>
		<li>
			<a href="<?php echo $this->_url('teacher/profile'); ?>">
				<div class="thumb"><img src="<?php echo $this->_asset('default/images/avatar.png'); ?>" alt="Avatar"></div>
				<h5>Saulo Duarte e Couto</h5>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem dolor consectetur veritatis distinctio repellendus animi beatae dolorem facere aperiam laudantium.</p>
			</a>
		</li>
	</ul>
</div>
