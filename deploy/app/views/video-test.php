<!-- Breadcrumbs -->
<div class="breadcrumbs">
	<ul>
		<li><a href="#">Início</a></li>
		<li><a href="#">Página Pai</a></li>
		<li>Página Atual</li>
	</ul>
</div>

<h3 class="heading-3">Vídeo-Teste</h3>

<!-- <div class="alert-main alert-main-success">
	<a href="#" class="close" title="Fechar alerta">x</a>
	<p><strong>Exemplo de alerta sucesso!</strong></p>
</div> -->

<div class="viewer-courses">
	<div class="wrapper m-bottom-30 a-right">
		<a href="#" target="_blank" class="link-button link-button-warning">Baixar Manual do Aluno (PDF, 342Kb)</a>
	</div>
	<header class="header">
		<h4>Aqui você verifica a compatibilidade do seu computador para assistir ao MP Cursos!</h4>
	</header>
	<div class="videoplayer">
		<div class="container">
			<!-- <video id="video-player-html" width="668" height="376" data-playlist="videos/vinheta.m4v|videos/introducao.mp4" poster="img/video-poster.jpg" type="video/mp4" controls></video> -->
			<object id="video-player" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="668" height="376">
				<param name="movie" value="<?php echo $this->_asset('default/movies/player/video_player8_xml.swf'); ?>">
				<param name="allowFullScreen" value="true">
				<!--[if !IE]>-->
				<object type="application/x-shockwave-flash" data="<?php echo $this->_asset('default/movies/player/video_player8_xml.swf'); ?>" width="668" height="376" allowfullscreen="true">
				<!--<![endif]-->
				<div class="no-videoplayer">
					<h1>Ops, vídeo não suportado! :(</h1>
					<p>Aparentemente seu navegador não possui os recursos básicos para rodar nosso player de vídeo.</p>
					<p>Atualize o Flash Player e tente novamente:</p>
					<p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
				</div>
				<!--[if !IE]>-->
				</object>
				<!--<![endif]-->
			</object>
		</div>
	</div>
	<div class="video-description">
		<div class="text-body">
			<p>Leia também os termos do <a href="#" target="_blank">Manual do Aluno</a>, ele é muito importante para entender todas as funcionalidades da nossa plataforma. Assista ao trecho da aula acima, é bem rápido! Após os testes e a leitura do Manual do Aluno, escolha seu curso e bons estudos!</p>

			<h4>Requisitos Mínimos</h4>
			<ul>
				<li>Conexão Internet 1MBps</li>
				<li>Navegadores recomendados: Google Chrome (29+), Mozilla Firefox (23+), Opera (12+) ou Internet Explorer (9+). Compatível com tablets e smatphones (iOS/Android/Windows Phone).</li>
				<li>Plug-In Flash Player versão 11.8 ou superior</li>
				<li>Software leitor de PDF (Recomendamos o Adobe Acrobat Reader)</li>
				<li>No Internet Explorer, ativar controle ActivexPlugin do Flash;</li>
				<li>Adobe Flash Player;</li>
				<li>Processador mínimo: Pentium III 500 Mhz;</li>
				<li>Memória RAM: mínimo 128 Mb (256Mb recomendável);</li>
				<li>Placa de Vídeo: SVGA com 8MB (16 MB recomendável);</li>
				<li>Placa de som: compatível com Microsoft® Windows® 98/2000/NT/XP;</li>
				<li>Espaço em disco (HD): 100 Mb;</li>
				<li>Tela com resolução de 1024x768 e 256 cores (mínima);</li>
			</ul>
			<p>O nosso sistema funciona em qualquer computador e conexão, desde que preencha os requisitos mínimos para execução.</p>
			<h5>NÃO ACONSELHAMOS A UTILIZAÇÃO DA TECNOLOGIA 3G, UMA VEZ QUE, A TAXA DE TRANSFÊNCIA DE EXIBIÇÃO DOS VÍDEOS, DIMINUI NO DECORRER DO MÊS, DIFICULTANDO O ACESSO DO ALUNO ÀS AULAS.</h5>
		</div>
	</div>
</div>

<div class="wrapper group-separate group-separate-fine m-top-30 a-right">
	<a href="#" class="link-button link-button-large link-button-success">Continuar Comprando</a>
</div>
